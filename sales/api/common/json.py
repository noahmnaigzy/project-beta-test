from json import JSONEncoder
from django.urls import NoReverseMatch
from django.db.models import QuerySet, Model
from datetime import datetime, date
from django.forms.models import model_to_dict

class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, (datetime, date)):
            return o.isoformat()
        else:
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            d = {}
            if hasattr(o, "get_api_url"):
                try:
                    d["href"] = o.get_api_url()
                except NoReverseMatch:
                    pass
            for property in self.properties:
                value = getattr(o, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                d[property] = value
            d.update(self.get_extra_data(o))
            return d
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}
class CustomJSONEncoder(JSONEncoder):
    def default(self, o):
        # Encoding for dates
        if isinstance(o, (datetime, date)):
            return o.isoformat()

        # Encoding for QuerySets
        if isinstance(o, QuerySet):
            # Convert the QuerySet to a list of dictionaries
            return [model_to_dict(item) for item in o]

        # Encoding for Django models
        if isinstance(o, Model):
            return model_to_dict(o)

        # Any other types of objects that aren't handled above will be
        # processed by the super class's default method.
        return super().default(o)
