from django.contrib import admin
from .models import AutomobileVO, Customer, Sale, Salesperson
# Register your models here.
admin.site.register(AutomobileVO)
admin.site.register(Customer)
admin.site.register(Sale)
admin.site.register(Salesperson)
