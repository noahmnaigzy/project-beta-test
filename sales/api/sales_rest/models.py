from django.db import models

# Create your models here.
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=30, unique=True)
    sold = models.BooleanField(default=False)

    def _str_(self):
        return self.vin

class Salesperson(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.PositiveIntegerField(unique=True)

class Customer(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=25, unique=True)

class Sale(models.Model):
    price = models.CharField(max_length=20)
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobile",
        on_delete=models.CASCADE
    )
    sales_person = models.ForeignKey(
        Salesperson,
        related_name="sales_person",
        on_delete=models.PROTECT,
    )
    customer =models.ForeignKey(
        Customer,
        related_name="customer",
        on_delete=models.PROTECT
    )
