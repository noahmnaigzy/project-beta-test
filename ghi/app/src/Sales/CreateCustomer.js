import React, {useEffect, useState } from 'react';


function CustomerForm() {

    const [ firstname, setFirstname] = useState('');
    const [ lastname, setLastName] = useState('');
    const [ address, setAddress] = useState('');
    const [ phonenumber, setPhonenumber] = useState('');
    const [ customers, setCustomers] = useState([]);
    const [confirmation, setConfirmation] = useState(false)

    async function loadCustomers() {
        const url = 'http://localhost:8090/api/customers/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setCustomers(data.customers);
        }
      }
      useEffect(() => {
        loadCustomers();
      }, [])

    async function handleSubmit(event) {
        event.preventDefault();
        setConfirmation(false)
        const data = {
          first_name: firstname,
          last_name:lastname,
          address,
          phone_number:phonenumber,


        };



    const customerUrl = 'http://localhost:8090/api/customers/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(customerUrl, fetchConfig);
    if (response.ok) {
      setConfirmation(true)
      setFirstname('');
      setLastName('');
      setAddress('');
      setPhonenumber('');
      loadCustomers();

    }
  }

  function handleChangeFirstName(event) {
    const { value } = event.target;
    setFirstname(value);
  }

  function handleChangeLastName(event) {
    const { value } = event.target;
    setLastName(value);
  }

  function handleChangeAddress(event) {
    const { value } = event.target;
    setAddress(value);
  }

  function handleChangePhoneNumber(event) {
    const { value } = event.target;
    setPhonenumber(value);
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Customer</h1>
          {confirmation && <div className="alert alert-success">Customer Created!</div>}
          <form onSubmit={handleSubmit} id="create-customer-form">
            <div className="form-floating mb-3">
              <input value={firstname} onChange={handleChangeFirstName} placeholder="First Name" required type="text" name="first name" id="first_name" className="form-control" />
              <label htmlFor="First Name">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={lastname} onChange={handleChangeLastName} placeholder="Last Name" required type="text" name="last name" id="last_name" className="form-control" />
              <label htmlFor="last name">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={address} onChange={handleChangeAddress} placeholder="Address" required type="text" name="address" id="address" className="form-control" />
              <label htmlFor="Address">Address</label>
            </div>
            <div className="form-floating mb-3">
              <input value={phonenumber} onChange={handleChangePhoneNumber} placeholder="Address" required type="text" name="phone number" id="phone_number" className="form-control" />
              <label htmlFor="phone number">Phone Number </label>
            </div>


            <button className="btn btn-primary">Create Customer</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CustomerForm;
