import React, { useEffect, useState } from 'react'

function ListSales() {
    const [sales, SetSales] = useState([]);

    async function loadSales() {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
            const data = await response.json();
            SetSales(data.sales)
        } else {
            console.error(response)
        }
    }


useEffect(() => {
    loadSales()
}, [])


return (
<div>
  <h1>Sales</h1>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Employee ID</th>
          <th>Sales Person Name</th>
          <th>Customer</th>
          <th>VIN</th>
          <th>Price</th>
        </tr>
      </thead>
      <tbody>
        {sales.map(sale => {
          return (
            <tr key={sale.id}>
              <td>{ sale.sales_person.employee_id}</td>
              <td>{ sale.sales_person.first_name}</td>
              <td>{ sale.customer.first_name} {sale.customer.last_name}</td>
              <td>{ sale.automobile.vin }</td>
              <td>{ sale.price }</td>

            </tr>
          );
        })}
      </tbody>
    </table>
</div>
  );

}

export default ListSales
