import React, { useState, useEffect } from "react";

function ListAutomobiles() {
  const [automobiles, setAutomobiles] = useState([]);

  const handleDeleteAutomobile = async (event, vin) => {
    event.preventDefault();

    const fetchOptions = {
      headers: {
        "Content-Type": "application/json",
      },
      method: "DELETE",
    };

    try {
      const response = await fetch(
        `http://localhost:8100/api/automobiles/${vin}/`,
        fetchOptions
      );
      if (response.ok) {
        await loadAutomobiles();
      }
    } catch (error) {
      console.error(error);
    }
  };

  async function loadAutomobiles() {
    try {
      const response = await fetch("http://localhost:8100/api/automobiles/");
      if (response.ok) {
        const data = await response.json();
        setAutomobiles(data.autos);
      } else {
        console.error(response);
      }
    } catch (e) {
      console.error("No automobiles found", e);
    }
  }

  useEffect(() => {
    loadAutomobiles();
  }, []);

  return (
    <>
      <h2>Automobiles</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Vin</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Sold</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {automobiles.map((auto) => (
            <tr key={auto.vin}>
              <td>{auto.vin}</td>
              <td>{auto.color}</td>
              <td>{auto.year}</td>
              <td>{auto.model.name}</td>
              <td>{auto.model.manufacturer.name}</td>
              <td>{auto.sold ? "Yes" : "No"}</td>
              <td>
                <a
                  onClick={(event) => handleDeleteAutomobile(event, auto.vin)}
                  type="button"
                  className="btn btn-link"
                >
                  Delete
                </a>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}

export default ListAutomobiles;
