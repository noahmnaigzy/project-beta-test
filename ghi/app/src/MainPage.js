import React from "react";
import { NavLink } from "react-router-dom";

function MainPage() {
  const gifUrl =
    "https://media.tenor.com/hgH0ropAo0oAAAAi/mario-kart-drifting.gif";

  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership management!
        </p>
        <img src={gifUrl} alt="Mario Kart Drifting GIF" />
      </div>
    </div>
  );
}

export default MainPage;
