import React, { useState, useEffect } from "react";

function ListVehicles() {
  const [vehicleModels, setVehicleModels] = useState([]);

  const handleDeleteVehicleModel = async (event, id) => {
    event.preventDefault();

    const fetchOptions = {
      headers: {
        "Content-Type": "application/json",
      },
      method: "DELETE",
    };

    try {
      const response = await fetch(
        `http://localhost:8100/api/models/${id}/`,
        fetchOptions
      );
      if (response.ok) {
        await loadVehicleModels();
      }
    } catch (error) {
      console.error(error);
    }
  };

  async function loadVehicleModels() {
    try {
      const response = await fetch("http://localhost:8100/api/models/");
      if (response.ok) {
        const data = await response.json();
        setVehicleModels(data.models);
      } else {
        console.error(response);
      }
    } catch (e) {
      console.error("No vehicle models found", e);
    }
  }

  useEffect(() => {
    loadVehicleModels();
  }, []);

  return (
    <>
      <h2>Models</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {vehicleModels.map((model) => (
            <tr key={model.id}>
              <td>{model.name}</td>
              <td>{model.manufacturer.name}</td>
              <td>
                <img src={model.picture_url} style={{ width: "100px" }} />
              </td>
              <td>
                <a
                  onClick={(event) => handleDeleteVehicleModel(event, model.id)}
                  type="button"
                  className="btn btn-link"
                >
                  Delete
                </a>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}

export default ListVehicles;
