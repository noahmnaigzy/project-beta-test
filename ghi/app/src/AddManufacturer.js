import React, { useState } from "react";

function AddManufacturer() {
  const [name, setName] = useState("");

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};

    data.name = name;

    const url = "http://localhost:8100/api/manufacturers/";
    const fetchOptions = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchOptions);
    if (response.ok) {
      const newManufacturer = await response.json();

      setName("");
    }
  };

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  return (
    <>
      <h1>Add a Manufacturer</h1>
      <form onSubmit={handleSubmit} id="create-technician-form">
        <div className="form-floating mb-3">
          <input
            value={name}
            onChange={handleNameChange}
            placeholder="name"
            required
            name="name"
            type="text"
            id="name"
            className="form-control"
          />
          <label htmlFor="name">Name...</label>
        </div>
        <button className="btn btn-primary">Add</button>
      </form>
    </>
  );
}
export default AddManufacturer;
