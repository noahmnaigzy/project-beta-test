import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          CarCar
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0 d-flex flex-row">
            {/* Models Dropdown */}
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="#"
                id="modelsDropdown"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Models
              </a>
              <ul className="dropdown-menu" aria-labelledby="modelsDropdown">
                <li>
                  <NavLink className="dropdown-item" to="/models">
                    Models
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/models/create">
                    Create a Model
                  </NavLink>
                </li>
              </ul>
            </li>

            {/* Automobiles Dropdown */}
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="#"
                id="automobilesDropdown"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Automobiles
              </a>
              <ul
                className="dropdown-menu"
                aria-labelledby="automobilesDropdown"
              >
                <li>
                  <NavLink className="dropdown-item" to="/automobiles">
                    Automobiles
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/automobiles/create">
                    Add an Automobile
                  </NavLink>
                </li>
              </ul>
            </li>
          </ul>

          {/* Other Dropdowns (Technician, Appointments, Manufacturers) */}
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            {/* Technician Dropdown */}
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="#"
                id="technicianDropdown"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Technician
              </a>
              <ul
                className="dropdown-menu"
                aria-labelledby="technicianDropdown"
              >
                <li>
                  <NavLink className="dropdown-item" to="/technicians/create">
                    Add Technician
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/technicians">
                    Technicians
                  </NavLink>
                </li>
              </ul>
            </li>

            {/* Sales Dropdown */}
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="#"
                id="moreDropdown"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Sales
              </a>
              <ul className="dropdown-menu" aria-labelledby="moreDropdown">
                <li>
                  <NavLink className="dropdown-item" to="/sales">
                    Sales
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/customers">
                    Customers
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/salespeople">
                    Salespeople
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/customers/new">
                    Add a Customer
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/salespeople/new">
                    Add a Salesperson
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/sales/new">
                    Add a Sale
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/salespeople/history">
                    Salesperson History
                  </NavLink>
                </li>
              </ul>
            </li>
            {/* End of Sales Dropdown */}

            {/* Appointments Dropdown */}
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="#"
                id="appointmentsDropdown"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Appointments
              </a>
              <ul
                className="dropdown-menu"
                aria-labelledby="appointmentsDropdown"
              >
                <li>
                  <NavLink className="dropdown-item" to="/appointments/create">
                    Create a Service Appointment
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/appointments">
                    Appointments
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/appointments/history">
                    Service History
                  </NavLink>
                </li>
              </ul>
            </li>
            {/* End of Appointments Dropdown */}

            {/* Manufacturers Dropdown */}
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="#"
                id="manufacturersDropdown"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Manufacturers
              </a>
              <ul
                className="dropdown-menu"
                aria-labelledby="manufacturersDropdown"
              >
                <li>
                  <NavLink className="dropdown-item" to="/manufacturers/create">
                    Create a Manufacturer
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/manufacturers">
                    Manufacturers
                  </NavLink>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
