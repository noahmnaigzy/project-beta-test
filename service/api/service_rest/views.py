from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
from .encoders import AppointmentListEncoder, TechnicianListEncoder
from .models import AutomobileVO, Appointment, Technician


@require_http_methods(["GET", "POST", "DELETE"])
def api_list_Technicians(request, id=None):
    if request.method == "GET":
        technician = Technician.objects.all()
        if not technician:
            return JsonResponse(
                {"message": "No techician found"},
                status=404,
            )
        return JsonResponse(
            {"technicians": technician},
            encoder=TechnicianListEncoder,
        )
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianListEncoder,
                safe=False,
            )
        except json.JSONDecodeError:
            return JsonResponse(
                {"message": "Invalid data requested"},
                status=400,
            )
    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=id)
            technician.delete()
            return JsonResponse({"deleted": True})
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician not found"},
                status=404,
            )


@require_http_methods(["GET", "POST", "PUT", "DELETE"])
def api_list_Appointments(request, id=None, vin=False):
    if request.method == "GET":
        appointment = Appointment.objects.all()
        if not appointment:
            return JsonResponse(
                {"message": "No appointment found"},
                status=404,
            )
        return JsonResponse(
            {"appointments": appointment},
            encoder=AppointmentListEncoder,
        )
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            appointment = Appointment.objects.get(id=id)
            props = ["status"]
            for prop in props:
                if prop in content:
                    setattr(appointment, prop, content[prop])
                appointment.save()
                return JsonResponse(
                    appointment,
                    encoder=AppointmentListEncoder,
                    safe=False,
                )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appt does not exist"})
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        content = json.loads(request.body)
        try:
            technician = content.get("technician")
            technician = Technician.objects.get(id=technician)
            technician = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid tech id"},
                status=400,
            )

        content["technician"] = technician
        try:
            vin = content["vin"]
            vip = AutomobileVO.objects.get(vin=vin)
            if vip:
                print("it's working")
                content["vip"] = True
                appointment = Appointment.objects.create(**content)
        except AutomobileVO.DoesNotExist:
            print("Auto doesn/t work")
            appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )
