from django.urls import path
from .views import api_list_Technicians, api_list_Appointments

urlpatterns = [
    path(
        "technicians/",
        api_list_Technicians,
        name="api_list_Technicians",
    ),
    path(
        "technicians/<int:id>/",
        api_list_Technicians,
        name="api_delete_Technicians",
    ),
    path(
        "appointments/",
        api_list_Appointments,
        name="api_list_Appointments",
    ),
    path(
        "appointments/<int:id>/",
        api_list_Appointments,
        name="api_delete_Appointments",
    ),
    path(
        "appointments/<int:id>/finish/",
        api_list_Appointments,
        name="api_finish_Appointments",
    ),
    path(
        "appointments/<int:id>/cancel/",
        api_list_Appointments,
        name="api_cancel_Appointments",
    ),
]
