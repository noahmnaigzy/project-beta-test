from .models import Technician, Appointment, AutomobileVO
from common.json import ModelEncoder
from django.core.serializers.json import DjangoJSONEncoder
from datetime import time


class TimeEncoder(DjangoJSONEncoder):
    def default(self, obj):
        if isinstance(obj, time):
            return obj.strftime("%H:%M:%S")
        elif obj is None:
            return None
        elif isinstance(obj, str):
            return obj
        return super().default(obj)


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
        "id",
    ]


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "time",
        "reason",
        "status",
        "vin",
        "customer",
        "id",
        "vip",
        "technician",
    ]

    encoders = {
        "technician": TechnicianListEncoder(),
        "time": TimeEncoder(),
    }
